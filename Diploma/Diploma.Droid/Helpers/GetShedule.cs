using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Android.Content.Res;
using Newtonsoft.Json;

namespace Diploma.Droid.Helpers
{
    public class GetShedule:Diploma.Model.DependencyInterfaces.IGetShedule
    {
        public string ReadShedule(string fileName)
        {
            string response;
            AssetManager assets = Android.App.Application.Context.Assets;
            StreamReader strm = new StreamReader(assets.Open(fileName));
            response = strm.ReadToEnd();
            return response;
        }

        public Model.SheduleModel.Department DeserializeShedule(string jsonText)
        {
            return JsonConvert.DeserializeObject<Model.SheduleModel.Department>(jsonText);
        }
    }
}