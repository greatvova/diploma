using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Diploma.Droid.Helpers
{
    public class Settings:Model.DependencyInterfaces.ISettings
    {
        public void SetGroupCode(string code)
        {
            var prefs = Android.App.Application.Context.GetSharedPreferences("SheduleInfo", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("GroupCode", code);
            prefEditor.Commit();
        }

        public string GetGroupCode()
        {
            var prefs = Android.App.Application.Context.GetSharedPreferences("SheduleInfo", FileCreationMode.Private);
            var result = prefs.GetString("GroupCode", string.Empty);
            return result;
        }
    }
}