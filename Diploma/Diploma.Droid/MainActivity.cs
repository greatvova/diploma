﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics.Drawables;

[assembly: Xamarin.Forms.Dependency(typeof(Diploma.Droid.Helpers.GetShedule))]
[assembly: Xamarin.Forms.Dependency(typeof(Diploma.Droid.Helpers.Settings))]

namespace Diploma.Droid
{
	[Activity (Label = "НУХТ", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);
			LoadApplication (new Diploma.App ());
            RemoveAppIconFromActionBar();
        }

        void RemoveAppIconFromActionBar()
        {
            var actionBar = ((Activity)this).ActionBar;
            actionBar.SetIcon(new ColorDrawable(Android.Graphics.Color.Transparent));
        }
    }
}

