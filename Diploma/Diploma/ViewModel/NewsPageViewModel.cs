﻿using MVVMBase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Diploma.ViewModel
{
    public class NewsPageViewModel:MVVMBase.BaseViewModel
    {
        public ICommand GoArticle        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    switch (Convert.ToInt32(executedParam))
                    {
                        case 0:
                            App.Navigation.PushAsync(new View.ArticlePage("Слово декана факультету АКС", "Форсюк Андрій Васильович - \nв.о.декана факультету автоматизації і комп’ютерних систем \n \nНеможливо сьогодні уявити сучасне виробництво без високого рівня автоматизації і комп’ютеризації всіх його ділянок.Це зумовлене бажанням не лише полегшити роботу працівникам, а й отримати конкурентноспроможну продукцію високої якості.Оскільки харчова промисловість, до складу якої входять десятки тисяч підприємств різноманітного профілю, є однією з провідних в Україні, то впровадження новітніх інформаційних технологій та сучасних систем управління тут відбувається особливо інтенсивно."));
                            break;
                        case 1:
                            App.Navigation.PushAsync(new View.ArticlePage("Перші сертифікати - вручено", "З жовтня 2016 року в нашому університеті було започатковане навчання за унікальними сертифікатними програмами. На нашиму факультеті теж пройшло навчання за сертифікатною освітньою професійною програмою «Моделювання та реінжиніринг бізнес-процесів». Керівник програми – к.т.н., доц.Загоровська Лариса Григорівна, менеджер – к.т.н., доц. Грибков Сергій Віталійович. \n26 грудня 2016 р.декан факультету Автоматизації і компютерних систем Маноха Людмила Юрівна урочисто вручила сертифікати першим слухачам сертифікатної програми. "));
                            break;
                        case 2:
                            App.Navigation.PushAsync(new View.ArticlePage("З Новим 2017 роком!", "Шановні студенти, аспіранти, науково-педагогічні працівники та співробітники факультету Автоматизації і комп'ютерних сисстем! \nПрийміть сердечні вітання з новорічними та Різдвяними святами – найсвітлішими та найрадіснішими днями, що несуть нам віру у власні сили, надії на кращі часи та світле майбутнє."));
                            break;
                        case 3:
                            App.Navigation.PushAsync(new View.ArticlePage("Брейн-ринг 2016", "09.12.16 року о 14:50 в #Studclub НУХТу відувся \"Брейн - ринг\"."));
                            break;
                        case 4:
                            App.Navigation.PushAsync(new View.ArticlePage("Вечір факультету АКС - 2016", "8 грудня у цьогорічному загальноуніверситетському фестивалі «День інституту/факультету» поставив крапку факультет автоматизації і комп’ютерних систем, який представив постановку «Бурлеск». "));
                            break;
                        case 5:
                            App.Navigation.PushAsync(new View.ArticlePage("ІІІ Міжнародна науково-технічна internet-конференція «Сучасні методи, інформаційне, програмне та технічне забезпечення систем управління організаційно-технічними та технологічними комплексами»", "23 листопада 2016 року на базі кафедри автоматизації та інтелектуальних систем керування факультету автоматизації і комп’ютерних систем відбулася ІІІ Міжнародна науково-технічна Internet-конференція «Сучасні методи, інформаційне, програмне та технічне забезпечення систем управління організаційно-технічними та технологічними комплексами»."));
                            break;
                        case 6:
                            App.Navigation.PushAsync(new View.ArticlePage("Міжнародний день", "17 листопада - Міжнародний день студентів!"));
                            break;
                        case 7:
                            App.Navigation.PushAsync(new View.ArticlePage("16 листопада відбудеться Ярмарок вакансій! 2016", "16 листопада у стінах Національного університету харчових технологій відбудеться Ярмарок вакансій."));
                            break;
                        case 8:
                            App.Navigation.PushAsync(new View.ArticlePage("АКС - володар Кубку ректора з баскетболу!!!", "АКС - володар Кубку ректора з баскетболу!!!"));
                            break;
                        case 9:
                            App.Navigation.PushAsync(new View.ArticlePage("Срібні призери Кубку першокурсника з баскетболу НУХТ 2016", "Збірна команда першокурсників АКС з баскетболу – срібні призери Кубку першокурсника з баскетболу НУХТ "));
                            break;
                        case 10:
                            App.Navigation.PushAsync(new View.ArticlePage("Гості на факультет Автоматизаціїї і комп’ютерних систем", "Студенти Волинського технікуму НУХТ завітали в гості на факультет Автоматизаціїї і комп’ютерних систем. "));
                            break;
                        case 11:
                            App.Navigation.PushAsync(new View.ArticlePage("Кубок першокурсника з баскетболу НУХТ 2016", "Кубок першокурсника з баскетболу"));
                            break;
                        case 12:
                            App.Navigation.PushAsync(new View.ArticlePage("Кубок першокурсника з волейболу НУХТ 2016", "Збірна команда першокурсників АКС з волейболу – призер Кубку першокурсника з волейболу НУХТ "));
                            break;
                        case 13:
                            App.Navigation.PushAsync(new View.ArticlePage("Володарі \"Кубку першокурсника з футзалу НУХТ\"", "Факультет АКС - володарі \"Кубку першокурсника з футзалу НУХТ\" "));
                            break;
                        case 14:
                            App.Navigation.PushAsync(new View.ArticlePage("Конкурс української пісні 2016", "Початок жовтня, а отже в нашому університеті – конкурс української пісні. 5 жовтня у актовій залі університету знову лунали ліричні й запальні українські пісні та бурхливі оплески й скандування глядачів, адже студенти, директори навчально-наукових інститутів і декани факультетів разом із викладачами та співробітниками прийшли, аби підтримати своїх конкурсантів. "));
                            break;
                        default:
                            break;
                    }

                },
                canExecutedParam => { return true; });
            }
        }
    }
}
