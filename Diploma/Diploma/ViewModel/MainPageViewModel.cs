﻿using MVVMBase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Diploma.ViewModel
{
    public class MainPageViewModel:MVVMBase.BaseViewModel
    {
        public ICommand GoTimetable
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    App.Navigation.PushAsync(new View.TimetablePage());
                },
                canExecutedParam => { return true; });
            }
        }
        public ICommand GoShedule
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    App.Navigation.PushAsync(new View.ShedulePage());
                },
                canExecutedParam => { return true; });
            }
        }
        public ICommand GoNews
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    App.Navigation.PushAsync(new View.NewsPage());
                },
                canExecutedParam => { return true; });
            }
        }
        public ICommand GoGroups
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    App.Navigation.PushAsync(new View.GroupsPage());
                },
                canExecutedParam => { return true; });
            }
        }
        public ICommand GoSettings
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    App.Navigation.PushAsync(new View.SettingsPage());
                },
                canExecutedParam => { return true; });
            }
        }
    }
}
