﻿using MVVMBase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Diploma.ViewModel
{
    public class ShedulePageViewModel:MVVMBase.BaseViewModel
    {
        public ICommand ChangeWeek
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    
                },
                canExecutedParam => { return true; });
            }
        }

        private ContentPage content;
        public ContentPage Content
        {
            get { return content; }
            set
            {
                content = value;
                OnPropertyChanged();
            }
        }

    }
}
