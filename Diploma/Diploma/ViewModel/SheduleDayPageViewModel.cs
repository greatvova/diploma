﻿using MVVMBase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Diploma.ViewModel
{
    public class SheduleDayPageViewModel:MVVMBase.BaseViewModel
    {
        public ICommand GoTimetable
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    App.Navigation.PushAsync(new View.TimetablePage());
                },
                canExecutedParam => { return true; });
            }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }

        private string consultation;
        public string Consultation
        {
            get { return consultation; }
            set
            {
                consultation = value;
                OnPropertyChanged();
            }
        }

        private Model.SheduleModel.Day day;
        public Model.SheduleModel.Day Day
        {
            get { return day; }
            set
            {
                day = value;
                OnPropertyChanged();
            }
        }


    }
}
