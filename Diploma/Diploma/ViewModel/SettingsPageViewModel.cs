﻿using MVVMBase;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Diploma.ViewModel
{
    public class SettingsPageViewModel: MVVMBase.BaseViewModel
    {
        public ICommand Save
        {
            get
            {
                return new DelegateCommand(executedParam =>
                {
                    DependencyService.Get<Model.DependencyInterfaces.ISettings>().SetGroupCode("");
                },
                canExecutedParam => { return true; });
            }
        }

        private int department = 0;
        public int Department
        {
            get { return department; }
            set
            {
                department = value;
                OnPropertyChanged();
            }
        }

        private int course = 0;
        public int Course
        {
            get { return course; }
            set
            {
                course = value;
                OnPropertyChanged();
            }
        }

        private int group;
        public int Group
        {
            get { return group; }
            set
            {
                group = value;
                OnPropertyChanged();
            }
        }
    }
}
