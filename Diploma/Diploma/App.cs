﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Diploma
{
	public class App : Application
	{
        public static INavigation Navigation;
		public App ()
		{
            // The root page of your application
            var groupCode = DependencyService.Get<Model.DependencyInterfaces.ISettings>().GetGroupCode();
            if (!String.IsNullOrEmpty(groupCode))
            {
                SetGroupByCode(groupCode);
            }
            

            MainPage = new NavigationPage(new View.MainPage());
            Navigation = MainPage.Navigation;
		}

        private void LoadShedule(string fileName)
        {
            Model.SheduleModel.Department department = new Model.SheduleModel.Department();
            string jsonText = DependencyService.Get<Model.DependencyInterfaces.IGetShedule>().ReadShedule(fileName);
            Model.SheduleModel.groupEntity = DependencyService.Get<Model.DependencyInterfaces.IGetShedule>().DeserializeShedule(jsonText).Cources[0].Groups[0];
        }

        public void SetGroupByCode(string groupCode)
        {
            switch (groupCode)
            {
                case "001":
                    LoadShedule("ACS_1_2.json");
                    break;
                default:
                    LoadShedule("Default.json");
                    break;
            }
        }

        protected override void OnStart ()
		{

            // Handle when your app starts
        }

        

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
