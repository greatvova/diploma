﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diploma.Model
{
    public class SheduleModel
    {
        public static Model.SheduleModel.Group groupEntity { get; set; }
        public class Lesson
        {
            public string Name { get; set; }
            public string Type { get; set; }
            public string Location { get; set; }
            public string Lecturer { get; set; }
        }

        public class Day
        {
            public int Number { get; set; }
            public string Name { get; set; }
            public List<Lesson> Lessons { get; set; }
        }

        public class Week
        {
            public int Number { get; set; }
            public List<Day> Days { get; set; }
        }
        public class Group
        {
            public string Name { get; set; }
            public List<Week> Shedule { get; set; }
        }

        public class Course
        {
            public string Name { get; set; }
            public List<Group> Groups { get; set; }
        }

        public class Department
        {
            public string Name { get; set; }
            public List<Course> Cources { get; set; }
        }
    }
}
