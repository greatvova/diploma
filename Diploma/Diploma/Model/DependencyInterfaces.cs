﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diploma.Model
{
    public class DependencyInterfaces
    {
        public interface IGetShedule
        {
            string ReadShedule(string fileName);
            Model.SheduleModel.Department DeserializeShedule(string jsonText);
        }

        public interface ISettings
        {
            void SetGroupCode(string code);
            string GetGroupCode();
        }
    }
}
