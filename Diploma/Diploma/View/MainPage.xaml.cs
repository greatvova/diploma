﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class MainPage : MasterDetailPage
	{
        ViewModel.MainPageViewModel model;
		public MainPage ()
		{
            model = new ViewModel.MainPageViewModel();
			InitializeComponent ();
            this.BindingContext = model;
            this.Master = new View.MenuPage();
            this.Detail = new View.ShedulePage();
		}
	}
}
