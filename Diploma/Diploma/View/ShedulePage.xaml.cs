﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class ShedulePage : CarouselPage
	{
        ViewModel.ShedulePageViewModel model;
        int activeWeek = 1;
		public ShedulePage ()
		{
            model = new ViewModel.ShedulePageViewModel();
			InitializeComponent ();
            this.BindingContext = model;
            
            var course = Model.SheduleModel.groupEntity;

            this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[0]));
            this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[1]));
            this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[2]));
            this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[3]));
            this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[4]));
            
            changeWeek.Clicked += (sender, e) =>
            {
                if(activeWeek == 1)
                {
                    this.Children.Clear();
                    this.Children.Add(new SheduleDayPage(course.Shedule[1].Days[0]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[1].Days[1]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[1].Days[2]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[1].Days[3]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[1].Days[4]));
                    activeWeek = 2;
                }
                else
                {
                    this.Children.Clear();
                    this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[0]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[1]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[2]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[3]));
                    this.Children.Add(new SheduleDayPage(course.Shedule[0].Days[4]));

                    activeWeek = 1;
                }
            };
        }
	}
}
