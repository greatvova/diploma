﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class SettingsPage : ContentPage
	{
        ViewModel.SettingsPageViewModel model;
		public SettingsPage ()
		{
            model = new ViewModel.SettingsPageViewModel();
			InitializeComponent ();
            this.BindingContext = model;

            var groupCode = DependencyService.Get<Model.DependencyInterfaces.ISettings>().GetGroupCode();
            
            if (!String.IsNullOrEmpty(groupCode))
            {
                Department.SelectedIndex = Convert.ToInt32(groupCode.Substring(0, 1));
                Course.SelectedIndex = Convert.ToInt32(groupCode.Substring(1, 1));
                Group.SelectedIndex = Convert.ToInt32(groupCode.Substring(2, 1));
            }

            Department.SelectedIndexChanged += (sender, e) =>
            {
                model.Department = Department.SelectedIndex;
            };

            Course.SelectedIndexChanged += (sender, e) =>
            {
                model.Course = Course.SelectedIndex;
            };

            Group.SelectedIndexChanged += (sender, e) =>
            {
                model.Group = Group.SelectedIndex;
            };

            SaveButton.Clicked += (sender, e) =>
            {
                DependencyService.Get<Model.DependencyInterfaces.ISettings>().SetGroupCode(model.Department.ToString() + model.Course.ToString() + model.Group.ToString());
                Diploma.App app = new Diploma.App();
                app.SetGroupByCode(model.Department.ToString() + model.Course.ToString() + model.Group.ToString());
            };            
		}
	}
}
