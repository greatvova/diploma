﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class GroupsPage : ContentPage
	{
        ViewModel.GroupsPageViewModel model;
		public GroupsPage ()
		{
            model = new ViewModel.GroupsPageViewModel();
			InitializeComponent ();
            this.BindingContext = model;
		}
	}
}
