﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Diploma.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SheduleDayPage : ContentPage
	{
        ViewModel.SheduleDayPageViewModel model;
		public SheduleDayPage (Model.SheduleModel.Day day)
		{
            model = new ViewModel.SheduleDayPageViewModel();
			InitializeComponent ();
            this.BindingContext = model;
            model.Day = day;            
        }
	}
}
