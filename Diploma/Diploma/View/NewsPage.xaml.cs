﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class NewsPage : ContentPage
	{
        ViewModel.NewsPageViewModel model;
		public NewsPage ()
		{
            model = new ViewModel.NewsPageViewModel();
			InitializeComponent ();
            this.BindingContext = model;
		}
	}
}
