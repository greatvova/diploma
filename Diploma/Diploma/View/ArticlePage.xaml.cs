﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class ArticlePage : ContentPage
	{        
		public ArticlePage (string inpTitle, string inpContent)
		{
			InitializeComponent ();
            Title.Text = inpTitle;
            Content.Text = inpContent;
		}
	}
}
