﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Diploma.View
{
	public partial class GroupPage : ContentPage
	{
		public GroupPage (string inpTitle, string inpContent)
		{
			InitializeComponent ();
            this.Title = inpTitle;
            Content.Text = inpContent;
		}
	}
}
